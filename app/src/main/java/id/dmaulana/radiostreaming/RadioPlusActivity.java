package id.dmaulana.radiostreaming;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class RadioPlusActivity extends Activity implements OnClickListener {

	
	private String url_radio= "http://108.61.30.179:2228";
    private ProgressBar playSeekBar;
    
    private TextView tvRadioUrl;
    private ImageView buttonPlay;

    private ImageView buttonStopPlay;

    private MediaPlayer player =null;
    private ListView listView1;
    private ArrayList<HashMap<String, String>> radiolist = new ArrayList<HashMap<String, String>>();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radio_plus);

        initializeUIElements();
        initData();
    }

    private void initializeUIElements() {

        playSeekBar = (ProgressBar) findViewById(R.id.progressBar1);
        playSeekBar.setMax(100);
        playSeekBar.setVisibility(View.INVISIBLE);
        playSeekBar.setIndeterminate(true);
        buttonPlay = (ImageView) findViewById(R.id.ImageViewPlay);
        buttonPlay.setOnClickListener(this);

        buttonStopPlay = (ImageView) findViewById(R.id.ImageViewPause);
        buttonStopPlay.setEnabled(false);
        buttonStopPlay.setOnClickListener(this);
        tvRadioUrl = (TextView) findViewById(R.id.textViewRadioUrl);
        tvRadioUrl.setText("RADIOUP - "+url_radio);
    }

    public void onClick(View v) {
        if (v == buttonPlay) {
            startPlaying(url_radio);
        } else if (v == buttonStopPlay) {
            stopPlaying();
        }
    }
    
    public void initData(){
    	radiolist.add(genHashMap("RADIOUP","http://108.61.30.179:2228"));
    	radiolist.add(genHashMap("Top 80","http://109.71.41.250:8086"));
    	radiolist.add(genHashMap("Nautic Radio","http://83.137.145.141:14240"));
    	radiolist.add(genHashMap("Jazz Groupe","http://91.121.25.89:8030"));
    	radiolist.add(genHashMap("Top 104","http://65.254.33.10:6600"));
    	setListData();
    }
    
    public HashMap<String,String> genHashMap(String name, String url){
    	HashMap<String,String> map = new HashMap<String, String>();
    	map.put("name", name);
    	map.put("url", url);
    	return map;
    }
    
    public void setListData(){
    	listView1 = (ListView) findViewById(R.id.listViewRadio);

		ListAdapter adapter = new SimpleAdapter(this, radiolist, R.layout.rowradioplus,
				new String[] { "name" }, new int[] {
						R.id.textViewRowRadioName });
		listView1.setAdapter(adapter);
		listView1.setOnItemClickListener(new OnItemClickListener() {
 			
         	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
         		//showToast("kesini", 0);
        		HashMap<String, String> radio = (HashMap<String, String>) listView1.getItemAtPosition(position);	        		
         	//	Toast.makeText(getBaseContext(), "ID '" + o.get("id") + "' was clicked.", Toast.LENGTH_SHORT).show();
        		tvRadioUrl.setText(radio.get("name")+" - "+radio.get("url"));
         		startPlaying(radio.get("url"));
 			}
 		});
 
    }

    private void startPlaying(String url) {
    	url_radio=url;
    	
    	  // initializeMediaPlayer();
    	stopPlaying();
        buttonStopPlay.setEnabled(true);
        buttonPlay.setEnabled(false);

        playSeekBar.setVisibility(View.VISIBLE);

        player.prepareAsync();
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(new OnPreparedListener() {

            public void onPrepared(MediaPlayer mp) {
                player.start();
            	
            }
        });
    }

    private void stopPlaying() {
    	if (player==null){
    		 initializeMediaPlayer();
    	}
        if (player.isPlaying()) {
            player.stop();
            player.release();
            initializeMediaPlayer();
        }

        buttonPlay.setEnabled(true);
        buttonStopPlay.setEnabled(false);
        playSeekBar.setIndeterminate(true);
        playSeekBar.setVisibility(View.INVISIBLE);
        
    }

    private void initializeMediaPlayer() {
        player = new MediaPlayer();
        try {
            player.setDataSource(url_radio);
        } catch (IllegalArgumentException e) {	        	
            e.printStackTrace();
        } catch (IllegalStateException e) {	        
        	e.printStackTrace();
        } catch (IOException e) {	        	
            e.printStackTrace();
        }

        player.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {

            public void onBufferingUpdate(MediaPlayer mp, int percent) {
            	 playSeekBar.setIndeterminate(false);
                playSeekBar.setSecondaryProgress(100);
                Log.i("Buffering", "" + percent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (player.isPlaying()) {
           // player.stop();
        }
    }

}